[Auf der Basis eines Projektes des iFSR an der TU Dresden](https://github.com/fsr/nopanic)

# The Manual

Erstiführer des Fachschaftsrates Informatik der Hochschule Zittau/Görlitz

Diese Broschüre möchte alle für Erstis wichtigen Informationen zusammentragen und damit auf lange Sicht vielleicht sogar die Unmengen von Flyern zu Beratungsangeboten und Ähnlichem ersetzen. Sie soll ein kurzer und präziser Leitfaden sein, der alles behandelt, was man als neuer Studierender wissen muss.

## Vorgehen zum Kompilieren / Erstellen des PDFs


1. (La-)TeX und benötigte Pakete installieren (bspws. MiKTeX, MacTex, etc.):
  * [awesomebox](https://ctan.org/pkg/awesomebox) (unter macOS in der Standard-TeX Installation enthalten. Alternativ kann man einfach die Datei `awesomebox.sty` im Projekt-Ordner hinterlegen.)
2. Benötigte Schriftarten besorgen. Unter Linux können diese z. B. nach `~/.fonts/` kopiert werden:
  * [Open Sans](https://www.fontsquirrel.com/fonts/open-sans) (auch im Paket `texlive-fonts-extra` enthalten)
  * LaTex sagt dir ansonsten welche Fonts dir noch fehlen. Einfach try and error and then try again :)
3. Mit LuaLaTeX / XeLaTeX das PDF erzeugen:
  `$ latexmk`  
  Alternativ kann man auch TeX manuell aufrufen, z. B. über:
  `lualatex manual.tex` oder `xelatex manual.tex` (Hint: LuaLaTeX ist wesentlich schneller!)
4. Wenn man eine komprimierte Variante erstellen möchte:  
  `gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=manual_compressed.pdf manual.pdf`
