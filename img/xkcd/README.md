# Comic Vectorization Workflow

The comics at https://xkcd.com are intended to be downloaded over the web and displayed on typical screens. Thus they are not at a suitable size for printing, especially the older ones.

Luckily, they are intended to look like they are drawn in clear, simple black strokes and seldomly have other coloured elements to them, making them ideal for vectorization. To achieve good results letting Inkscape trace it automatically, an upscaling solution like the Open Source UpscalerJS can be used.

## Vectorizing Comics With Only Black Strokes

1. Upscale comic at https://www.upscaler.ai/ (GAN 4×)
2. Trace bitmap in Inkscape (*single scan, brightness cutoff: 0.400, speckles: unchecked, smooth corners: 1.0, optimize: 0.20*), delete the original image and save a copy as PDF
3. Convert colours to CMYK with Scribus (*Edit → Colours and Fills → FromPDF#000000 → Delete → Replace With: Black → OK* | and if shown also *FromPdf#ffffff → Delete → Replace With: White → OK* | *OK*) and export as the same PDF (ensure *Output Intended For* in the *Colours* tab of the export dialog is set to *Greyscale*)

You might vary the brightness cutoff to make it look right.

## Vectorizing Comics With Colours

It's not as easy when there are more colours involved. In the case of `vomiting-emoji.png`, I first traced it as described above, deleted everything from the resulting path that was not meant to be black, and proceeded to trace a version of the image with all black lines removed (`vomiting_emoji-x4-noblack.png`) using *multiple scans* based on *Colours*, with only *stack* and *remove background* enabled. I chose *32* scans because it looked best and reducing it to my estimate of the total colour count didn't yield satisfying results. Afterwards, I compared the result to the original image and re-picked some colours that weren't saturated enough from it.

When adjusting the colours for print in Scribus, it's usually fine to just go through each colour, click *Edit*, switch it from *RGB* to *CMYK* and click *OK* without modifying any sliders. To make them less muddy you might also drop the black (*K*) a bit if you feel confident in not messing up and don't mind the extra work. When exporting, you need to make sure *Output Intended For* in the *Colours* tab is set to *Printer*.
