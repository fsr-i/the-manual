\addchap{Studienalltag}

Mit dem Studium kommen neue Aufgaben und Herausforderungen auf dich zu, die es zu meistern gilt.
Die folgenden Seiten geben dir einen kleinen Eindruck davon, wie das Studium aufgebaut ist und wie das Lernen an Uni und Hochschule funktioniert.
Lies dir aber auch deine \textbf{Studienordnung}~\link{https://web1.hszg.de/modul_St_Dokumente//IIb_SO_Lesefassung_2020.pdf} und \textbf{Prüfungsordnung}~\link{https://web1.hszg.de/modul_St_Dokumente//IIb_PO_Lesefassung_2020.pdf} durch.\\
Einen Überblick über die zeitliche Organisation deines Semesters bietet dir der Studienjahresablaufplan~\link{https://www.hszg.de/fileadmin/Redakteure/Hochschule/Studium/durchs_Studium/Studienverlauf/SJAP_2023_24_Web.pdf}.%~\link{https://www.hszg.de/hochschule/aktuelles/studienjahresablaufplan}.

\minisec{Module}

Im Verlauf deines Studiums musst du zahlreiche sogenannte Module erfolgreich absolvieren.
Ein Modul kann mehrere Lehrveranstaltungen beinhalten.
Das können Vorlesungen, Übungen/Seminare (an der HSZG unterscheiden wir dazwischen kaum) oder später auch Praktika oder Blockveranstaltungen sein, die auf einen kurzen Zeitraum konzentriert stattfinden.\\
Viele Module bestehen nur aus einer Vorlesung und einer dazugehörigen Übung.
Du schließt ein Modul ab, indem du die \textbf{Modulprüfung} bestehst.
Eine Modulprüfung kann sich aus einer oder mehreren Prüfungsleistungen (z.~B.~Klausur) zusammensetzen.
Manchmal muss zunächst eine Prüfungsvorleistung erbracht werden, um überhaupt an einer Prüfung teilnehmen zu dürfen.\\
Jedes Modul hat eine ausgeschriebene Anzahl an Leistungspunkten (LP, oft auch Credits oder ECTS-Punkte genannt).
Dabei entspricht ein ECTS-Punkt einer Arbeitsbelastung von 30~Stunden.
Wenn ein Modul 5~ECTS bringt, heißt das also, dass über das Semester verteilt 150~Stunden Arbeit anstehen.
Diese Arbeitsbelastung setzt sich zusammen aus Präsenzzeit (Zeit, die du tatsächlich in Vorlesungen/Übungen an der Uni verbringst),
Zeit zur Vor- und Nachbereitung der Veranstaltungen (Selbststudium), Prüfungsvorbereitung und der Prüfung selbst.
Die ECTS-Punkte für ein Modul werden nach bestandener Modulprüfung anerkannt.\\
Weiterhin wirst du auf die Abkürzung \textbf{SWS} stoßen.
SWS steht für Semesterwochenstunden und gibt den Zeitaufwand für eine Lehrveranstaltung an.
SWS treffen dabei lediglich eine Aussage über die Präsenzzeit an der Uni.
Die Zeit zur Vor- und Nachbereitung wird dabei nicht berücksichtigt.
Die Angabe 1~SWS bedeutet, dass die Lehrveranstaltung während der Vorlesungszeit wöchentlich durchschnittlich 45~min lang gelehrt wird.
Eine Lehrveranstaltung mit 4~SWS wird entsprechend pro Woche 3~Zeitstunden gelehrt.
Eine Lehreinheit an der Uni dauert 90~Minuten und wird Doppelstunde (DS) genannt.
Eine Veranstaltung mit 4~SWS findet also 2-mal wöchentlich statt.

Welche Module in deinem Studium vorkommen und welche zuvor erklärten Aspekte dafür jeweils gelten, findest du (mehr oder weniger) übersichtlich im \textbf{Modulkatalog}~\link{https://web1.hszg.de/modulkatalog/index.php?activTopic=4}.%~\link{https://web1.hszg.de/modulkatalog/index.php?activTopic=3\&activNav=2\&stid=566\&frei=1\&kennz=suche}.

\minisec{Stundenplan}

Der Modulkatalog empfiehlt dir für jedes Semester, an bestimmten Modulen teilzunehmen.
Dem solltest du auch so folgen.
Während man sich an anderen Unis für bestimmte Lehrveranstaltungen einschreiben muss, arbeitet die HSZG selbst für alle (in der Regel) konfliktfreie Stundenpläne aus.
Da wir auch nicht so hohe Studierendenzahlen haben, gibt es für Übungen eh nur ein Angebot, das meist sogar der Professor selbst hält.\\
Zum Stundenplan kommst du hier~\link{https://timetable.hszg.de/}.
In einem lustigen Dropdown kannst du dann dein Matrikelkürzel suchen, z.~B.~IIb24, und dir das ganze bei Bedarf statt als Liste als Timetable übersichtlicher darstellen lassen.
Die angegebenen Zahlen für "Wochen" bezeichnen hierbei die \emph{Kalenderwochen}, in denen der Eintrag stattfindet.

\begin{figure}[h!]
	\centering
	\hspace{-5mm}%
	\includegraphics[width=\dimexpr\textwidth+5mm]{img/stundenplan-iib24.png}
	\caption*{\small\textit{Stundenplan~{\link{https://timetable.hszg.de/}} für IIb24 (Stand: 17.09.2024)}}
\end{figure}

\minisec{Vorlesung}

In der Vorlesung wird der Stoff vermittelt, der schlussendlich in der Prüfung abgefragt wird.
Es ist also sinnvoll, die Vorlesung aktiv zu verfolgen und sie ggf. vor- und nachzubereiten.
Nicht ratsam ist es, erst vor der Prüfung den ganzen Stoff aufzuholen, da die Stoffmenge in der Regel sehr groß ist und das also unnötig mehr Stress in der Prüfungsphase bedeuten würde.\\
Je mehr Leute aber in einer Vorlesung sitzen, desto wahrscheinlicher ist es, dass jemand an einer Stelle in der Vorlesung nicht mitkommt.
Das kann jedem mal passieren.
Falls du also dieser jemand sein solltest, habe keine falsche Scheu, dem Dozierenden eine Frage zu stellen, um die Unklarheiten zu beseitigen.
Ja, auch, wenn es eine Verständnisfrage ist.
Aktive Beteiligung in der Vorlesung ist immer gerne gesehen und manche Dozierenden leben dabei erst richtig auf.
Wahrscheinlich freut sich dann auch der ein oder andere Kommilitone von dir, der dieselbe Frage --~aber nicht den Mut~-- hatte, zu fragen.

\minisec{Übung und Seminar}

Übungen werden zu fast allen Vorlesungen angeboten und dienen klassisch dazu, Aufgaben zum aktuellen Vorlesungsstoff zu bearbeiten.
Klausuren orientieren sich häufig an den Übungsaufgaben, deshalb solltest du die Übungen regelmäßig besuchen und noch vor der nächsten nacharbeiten, konntest du doch einmal nicht teilnehmen.
Seminare sind davon kaum zu unterscheiden.
Manchmal gibt es hier Vorträge, die ihr in irgendeiner Form selber erarbeiten und halten müsst.
\\
An der HSZG werden die Übungen und Seminare tatsächlich auch oft vom Professor selbst gehalten.
Das kann dazu führen, dass du selbst bei erneutem Nachfragen nur den gleichen Bahnhof verstehst, wie schon in der Vorlesung.
In dem Fall frag in der Pause und im späteren Tagesverlauf mal in deinem Matrikel, ob andere das besser verstanden haben, oder deine Kommilitonen höherer Semester.
Die müssen die Prüfung schließlich geschafft haben!
Bekanntlich versteht man viele Dinge besser, wenn man sie noch einmal aus einem anderen Mund erklärt bekommt.

\minisec{Praktikum}

Ein halbes Praktikumssemester gibt es für Bachelor Informatik und Bachelor Wirtschaft und Informatik im letzten, dem 6.~Semester.
Die andere Hälfte gehört dann der Bachelorarbeit.

\refstepcounter{dummy}\label{sec:pruefungen}
\minisec{Prüfungen}

Direkt an die Vorlesungszeit schließt die Prüfungszeit an.
Die dauert meist um die drei Wochen und wird dann vom Nachprüfungszeitraum abgelöst.
Und danach heißt es endlich: Semesterferien!

An welchen Daten diese Zeiträume im jeweiligen Studienjahr sind, findest du im Studienjahresablaufplan~\link{https://www.hszg.de/fileadmin/Redakteure/Hochschule/Studium/durchs_Studium/Studienverlauf/SJAP_2023_24_Web.pdf}.

Die genauen Prüfungstermine findest du auf derselben Seite, wo du auch deinen Stundenplan findest~\link{https://timetable.hszg.de/}.
In einem lustigen Dropdown kannst du wieder dein Matrikelkürzel suchen und die Kalenderwochen auswählen, in denen Prüfungen stattfinden.

Du bist automatisch für deine Prüfungen angemeldet.
Welche genau das sind, kannst du auf deinem Profil~\link{https://service.hszg.de/} unter \menu[;]{Prüfungsverwaltung;Info über an- und abgemeldete Prüfungen;Abschluss 84 Bachelor;Informatik (PO-Version 2020)} nachsehen.
Das ist meistens noch nicht am Anfang des Semesters eingetragen, aber spätestens dann, wenn du dir anfängst, über die Prüfungen Sorgen zu machen.

Du kannst dich von ebendiesen Prüfungen aber auch abmelden, und zwar bis zu 2~Wochen vor Beginn des Prüfungszeitraums.
Natürlich darfst du auch an zusätzlichen Prüfungen teilnehmen, auch dafür kannst du bis zu 2~Wochen vor dem Prüfungszeitraum einen Antrag stellen.
Du kannst zum Beispiel einen Freiversuch wagen, wenn du dich einem Thema aus einem höheren Semester auch ohne Besuch der Lehrveranstaltung gewappnet fühlst.
Du musst bloß aufpassen, dass das Modul als Vorleistung nicht gerade z.~B. ein Projekt hat, das über das ganze Semester gelaufen ist.
Die resultierende Note wird nur gezählt, wenn sie dir gefällt.

Solltest du an einer Prüfung kurzfristig doch nicht teilnehmen können (als Gründe dafür werden z.~B. Krankheit, Unfall oder Kindespflege akzeptiert), dann gibt es auch dafür einen Antrag~\link{https://www.hszg.de/fileadmin/Redakteure/Hochschule/Studium/durchs_Studium/Downloads/Dezernat_Studium_und_Internationales_-_Antraege_Formulare/Prüfungsamt/Antrag_Krankmeldung.pdf}.
Dieser muss innerhalb einer Woche nachgereicht werden.

Wenn du eine Prüfung mal nicht bestehst, also eine Note von 5,0 bekommst, wirst du automatisch in eine Wiederholungsprüfung eingeschrieben.
Diese liegt im Wiederholungsprüfungszeitraum, der am Anfang des nächsten Semesters stattfindet.
Solltest du danach noch eine weitere Wiederholungsprüfung brauchen, dann musst du dich dafür selber anmelden,
und zwar innerhalb eines Monats, mit diesem Antrag~\link{https://www.hszg.de/fileadmin/Redakteure/Hochschule/Studium/durchs_Studium/Downloads/Dezernat_Studium_und_Internationales_-_Antraege_Formulare/Prüfungsamt/Antrag_zweite_WP.pdf}.
Dieser Drittversuch ist dann leider auch dein letzter Versuch.
Wenn du dann auch nicht bestehst, wirst du exmatrikuliert.

Ausführliche Informationen zur Prüfungsordnung und alle Formulare findest du auf der Seite des Prüfungsamts~\link{https://www.hszg.de/studium/dein-weg-durchs-studium/pruefungsamt}.
Bei komplizierteren Fragen und Problemen kannst du dich auch direkt an Frau Köhler wenden, deren Kontaktdetails dort und auf~\fancypageref{sec:pruefungsamt} hinterlegt sind.

%\minisec{Leistungsnachweis}
%
%Bei manchen Prüfungen erhältst du neben der Note einen Leistungsnachweis (oder kurz: Schein).
%Dazu zählen unter anderem die Sprachkurse, die Forschungslinie und z.T. Nebenfachprüfungen.
%Diese Scheine brauchst du, um dir diese Leistungen im Prüfungsamt anrechnen lassen zu können.

%\refstepcounter{dummy}\label{sec:sprachausbildung}
%\minisec{Sprachausbildung}
%
%Es werden an der TU Dresden Kurse für fast alle möglichen (und unmöglichen) Sprachen angeboten.
%Zu diesem Zweck gibt es zwei Zentren für die Sprachausbildung:
%Das \enquote{Lehrzentrum Sprachen und Kulturen} (LSK) und \enquote{TUD Institute of Advanced Studies} (TUDIAS).
%Das Sprachangebot der beiden Einrichtungen ähnelt sich sehr stark.
%Du hast für diverse Sprachkurse ein Budget an Semesterwochenstunden (insgesamt 10 SWS), die du ausgeben kannst, wie du willst.
%Für dein Studium zum Bachelor der (Medien-)Informatik sind Sprachkurse generell optional, aber auf jeden Fall empfehlenswert.
%Für Diplomstudenten sind 2 Semester Englisch im Laufe des Studiums Pflicht.
%Studierst du allerdings Bachelor Informatik und möchtest danach mit dem Master Informatik an der TU Dresden weitermachen, wirst du für den Master das Sprachniveau B2 in Englisch nachweisen müssen,
%also kann es sich auch für dich anbieten, die entsprechenden Sprachkurse zu besuchen.
%Die Einschreibung für einen Sprachkurs erfolgt online~\link{https://sprachausbildung.tu-dresden.de} mit deinem ZIH-Login.
%Sobald die Kurse freigeschaltet sind, solltest du dich jedoch stark beeilen, denn die beliebten Kurse sind meist innerhalb weniger Minuten voll.
%Weitere Infos findest du unter~\link{https://tu-dresden.de/lsk/lskonline}~und~\link{https://www.tudias.de/}.

\minisec{Stipendien}

Neben dem \bafog\ sind auch Stipendien eine gern genutzte Möglichkeit der Studienfinanzierung.

Viele Stipendien werden von den 13 überwiegend staatlich finanzierten Begabtenförderungswerken vergeben, die sich hinsichtlich ihres weltanschaulichen, religiösen oder politischen Profils unterscheiden.
Oftmals werden Studierende aufgrund guter Leistungen von Schulen, Prüfungsämtern oder Hochschullehrern direkt vorgeschlagen.
Bei vielen Werken sind aber auch Selbstbewerbungen möglich.
Für die Aufnahme muss man keineswegs ein Überflieger sein.

Im Auswahlprozess können gesellschaftliches oder soziales Engagement eine ebenso wichtige Rolle spielen.
Die Stipendien werden in Anlehnung an das \bafog\ abhängig vom eigenen Einkommen und Vermögen sowie vom Einkommen der Eltern berechnet.
Zusätzlich erhalten die Stipendiaten eine monatliche Studienkostenpauschale in Höhe von 300 Euro.
Im Gegensatz zum \bafog\ müssen die Stipendien jedoch nicht zurückgezahlt werden.
Neben der finanziellen Förderung bieten alle Werke eine umfangreiche ideelle Förderung in Form von Sprachkursen, Exkursionen und Akademien.

Ebenso bekannt ist das Deutschlandstipendium, welches zur Hälfte von privaten Geldgebern finanziert wird.
Die finanzielle Förderung erfolgt unabhängig vom eigenen Einkommen oder dem der Eltern und beläuft sich auf 300 Euro monatlich.
Das Deutschlandstipendium wird nicht auf das \bafog\ angerechnet und muss ebenfalls nicht zurückgezahlt werden.
Ihr könnt euch jeweils im Juli direkt online~\link{https://www.hszg.de/hochschule/ueber-uns/deutschlandstipendium/fuer-studierende} bewerben.

Es gibt noch viele weitere Organisationen, deren Förderung überwiegend privat finanziert wird.
Diese vergeben jedoch oft nur wenige Vollstipendien oder beschränken die Förderung auf geringere Sach- oder Geldleistungen.
Achte auch auf Stipendien wie DAAD~\link{https://www.daad.de/de/} oder Stipendium Plus~\link{https://www.stipendiumplus.de/startseite.html}. Die helfen dir nicht nur in finanzieller Hinsicht, sondern auch dabei, deine Talente zu verbessern und dich für wissenschaftliche Arbeit zu begeistern. Achte bei der Bewerbung auf die Bewerbungsfristen und -voraussetzungen. Es ist empfehlenswert, alles im Voraus vorzubereiten, da du in manchen Fällen eine große Anzahl von Dokumenten vorbereiten muss.

%\begin{figure}[b]
%	\centering
%	%\includegraphics[width=\textwidth]{img/xkcd/zealous_autoconfig.png}
%	\includegraphics[width=\textwidth]{img/xkcd/zealous_autoconfig.pdf}
%	\caption*{{\small \textit{I hear this is an option in the latest Ubuntu release. (https://xkcd.com/416)}}}
%\end{figure}

\minisec{Hochschulgruppen}
\label{sec:hsg}

Lernen und feiern reicht dir nicht?
Such dir eine Hochschulgruppe!
Dort findest du ehrenamtlich engagierte Studierende, die aktiv das Leben auf dem Campus mitgestalten wollen.
Und je nachdem was du suchst, wirst du auch Diskussionen, die Möglichkeit anderen zu helfen, neue Erfahrungen, interessante Leute und vieles mehr finden können.
Eine Auflistung findest du auf der Seite des StuRa~\link{https://stura.hszg.de/service/hochschulgruppen}.
Vielleicht ist ja was für dich dabei.

\minisec{Auslandsaufenthalte}

Bei akutem Auslandswunsch oder studienbegleitendem Internationalisierungsdrang fragen Sie bitte das Auslandsamt oder Ihren Professor des Vertrauens.
Auslandsaufenthalte können Nebenwirkungen hervorrufen.
Über diese können Sie sich bei Kommilitonen erkundigen.

Im Verlaufe deines Studiums werden dich immer wieder Leute fragen, ob du nicht ein oder zwei Semester im Ausland verbringen möchtest.
Nun magst du dich als unschuldiger Erstsemester fragen, warum denn das und wieso kommt ihr jetzt schon damit an?
Die Antwort ist einfach:
Zum einen kannst du direkt deine Sprachkenntnisse verbessern, Kontakte knüpfen und neue Kulturen erleben --
Eine willkommene Abwechslung, um den Kopf frei zu bekommen nach den anstrengenden Semestern.
Es werden neue Perspektiven vermittelt, sowohl akademisch als auch fachlich.
Außerdem hilft es dir bei der Entwicklung deiner Softskills wie Selbstständigkeit, Toleranz und Anpassungsfähigkeit (um nur einige zu nennen).
Alles Dinge, die dir später weiterhelfen werden und für die du später dankbar sein wirst.
Kurz und knapp gesagt: Ein Auslandsaufenthalt ist nützlich, erfordert aber etwas Planung.
Deshalb ist es von Vorteil, sich möglichst früh zu informieren.
Für Infos und bei Fragen kannst du dich an das Akademische Auslandsamt der HSZG~\link{https://www.hszg.de/international/service/international-office} wenden und auf den Seiten der Hochschule~\link{https://www.hszg.de/international/wege-ins-ausland/studium-im-ausland} informieren.
Wenn man mutig ist, kann man sogar Professoren direkt fragen, ob sie Kontakte zu anderen Unis oder Unternehmen haben.
Es liegt an dir, wie erfolgreich dein Auslandsaufenthalt wird.
Ob beim Streicheln von Robben vor Neufundland oder beim Scrum-Meeting im Silicon Valley, es gibt eine Menge Angebote, die auf dich warten!

\minisec{Urlaubssemester}

Es gibt eine ganze Reihe von Gründen, die dich daran hindern können, dein Studium an der HSZG ordnungsgemäß weiterzuführen.
Klassische Gründe sind schwere Erkrankungen, längere Praktika, Auslandsaufenthalte oder (unverhoffter) Nachwuchs, um den du dich kümmern musst.
In solchen Fällen kannst du dich von deinem Studium beurlauben lassen, um dich voll und ganz auf den Urlaubsgrund zu konzentrieren.
Während eines Urlaubssemesters bist du von der Pflicht befreit, Prüfungen ablegen zu müssen, genießt aber weiterhin alle Vorteile des Studierendendaseins.
Andererseits hast du in dieser Zeit meist auch keinen Anspruch auf \bafog- oder Kindergeld-Zahlungen, also plane das nicht unbedingt bei deinen Einkünften ein.

Solltest du ein Auslandsstudium machen und ein paar Semester in fernen Landen verweilen, kannst du dir danach erbrachte Leistungen und Prüfungen hier anrechnen lassen.
Einziger Fallstrick dabei:
Wenn du genügend Leistungspunkte einbringst, wirst du trotzdem ein Fachsemester hochgestuft, aber da kann dir die Studienberatung~\fancypageref{sec:studienberatung} weiterhelfen.

Beantragen kannst du ein Urlaubssemester beim Zulassungsamt~\link{https://www.hszg.de/studium/dein-weg-durchs-studium/zulassungsamtstudierendensekretariat} während der Rückmeldefrist für das nächste Semester.
Wie genau das geht und weitere Informationen findest du unter~\link{https://www.hszg.de/studium/dein-weg-durchs-studium/zulassungsamtstudierendensekretariat/urlaubssemester}.

\vfill
\vfill
\vfill
\begin{figure}[h!]
	\centering
	%\includegraphics[scale=.45]{img/xkcd/vomiting_emoji.png}
	\includegraphics[width=\textwidth]{img/xkcd/vomiting_emoji.pdf}
	\caption*{{\small \textit{My favorite might be U+1F609 U+1F93F WINKING FACE VOMITING\@. (https://xkcd.com/1813)}}}
\end{figure}
\vfill

\minisec{Campus App MyHSZG}

Die Campus App MyHSZG ist ein Sammelplatz für Informationen über die Hochschule und dein Studium.
Hier kannst du dich über unterschiedliche Neuigkeiten oder Events an der Hochschule informieren oder auch das Angebot in der Mensa ansehen.
Zusätzlich kannst du auch persönliche Informationen abrufen, welche normalerweise über die Webseite der Hochschule, das Studierendenportal oder den E-Campus erreichbar sind.
So kannst du zum Beispiel deine Noten einsehen, deine Mails lesen oder deine ausgeliehenen Bücher aus der Hochschulbibliothek verwalten.
Daneben gibt es auch noch ein paar allgemein nützliche Funktionen, wie eine Übersicht der Gebäude auf dem Campus in Zittau und Görlitz, einen eingebauten Kalender oder, falls dir langweilig wird, einige Minispiele wie Schiffe versenken oder Basketball.
Also, falls du dich mal wieder in das Studierendenportal eingeloggt hast, obwohl du eigentlich in den E-Campus musstest, erinnere dich an die MyHSZG App, die dir den extra Weg ersparen könnte.


